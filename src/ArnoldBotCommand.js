
let _name = new WeakMap();
let _descr = new WeakMap();
let _usage = new WeakMap();
let _adminOnly = new WeakMap();

class ArnoldBotCommand {
    constructor(name, description, usage, adminOnly) {
        if(!usage) {
            usage = `!${name}`;
        }
        _name.set(this, name);
        _descr.set(this, description);
        _usage.set(this, usage);
        _adminOnly.set(this, adminOnly);
    }

    getName() {
        return _name.get(this);
    }

    getDescr() {
        return _descr.get(this);
    }

    getUsage() {
        return _usage.get(this);
    }

    isAdminOnly() {
        return _adminOnly.get(this);
    }
}

module.exports = ArnoldBotCommand;