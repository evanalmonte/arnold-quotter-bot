const ArnoldBotCommand = require('./ArnoldBotCommand.js');

let Command = Object.freeze({
    HELP: new ArnoldBotCommand("help", "Shows a list of available commands."),
    MOTIVATE: new ArnoldBotCommand("motivate", "Sends a motivational quote by Arnold Schwarzenegger to the current channel."),
    LARRY: new ArnoldBotCommand("larry", "You know what this does..."),
    AUTO_MOTIVATE: new ArnoldBotCommand("automotivate", "Sends a motivational quote by Arnold Schwzeneggar every X seconds to the current channel.", `!automotivate X`, true),
    STOP_MOTIVATING: new ArnoldBotCommand("stopmotivating", "Stop the auto motivate command.", "!stopmotivating", true)
});

class CommandInterpreter {
    /**
     * @return {Readonly<{HELP: ArnoldBotCommand, MOTIVATE: ArnoldBotCommand, LARRY: ArnoldBotCommand, AUTO_MOTIVATE: ArnoldBotCommand, STOP_MOTIVATING: ArnoldBotCommand}>}
     */
    static get Command() {
        return Command;
    }
}

module.exports = CommandInterpreter;