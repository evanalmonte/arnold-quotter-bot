const Discord = require("discord.js");
const client = new Discord.Client();
const CommandInterpreter = require("./CommandInterpreter.js");
const ArnoldQuoteRetriever = require("./ArnoldQuoteRetriever.js");
const config = require('./config.js');
const serverAutoMotivateMap = {};


client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}`);
    client.user.setActivity("!help", {type: "LISTENING"});
});

client.once("guildCreate", (guild) => {
    guild.channels.find(
        (channel) => channel.name === "general" && channel.type === "text"
    ).send(config.serverGreeting);
});

client.on("message", (message) => {
    if (message.author.bot) {
        return;
    } else if (message.channel.type === "dm") {
        if(message.author.id == "174639403522588682") {
            message.author.send("I LOVE YOU LARRY.");
        } else if (message.author.id == "495086318636892161") {
            message.author.send("You sexy bitch larry.");
        } else {
            message.author.send("Commands cannot be used within a DM.");
        }
        return;
    } else if (!message.content.startsWith("!")) {
        return;
    }
    const arguments = message.content.substring(1, message.content.length).split(" ");
    arguments[0] = arguments[0].toLowerCase();
    let selectedCommand = {};
    Object.keys(CommandInterpreter.Command).forEach(command => {
        if (CommandInterpreter.Command[command].getName() === arguments[0]) {
            selectedCommand = CommandInterpreter.Command[command];
        }
    });
    switch (selectedCommand) {
        case CommandInterpreter.Command.HELP:
            const helpDM = new Discord.RichEmbed().setColor(0x1D8286);
            const availableCommands = Object.getOwnPropertyNames(CommandInterpreter.Command);
            helpDM.setTitle("**Available Commands**");
            for (let i = 0; i < availableCommands.length; ++i) {
                let commandName = availableCommands[i];
                let currentCommand = CommandInterpreter.Command[commandName];
                if (currentCommand.isAdminOnly() && (message.guild.ownerID !== message.author.id)) {
                    continue;
                }
                helpDM.addField(
                    `${currentCommand.getName()}`,
                    `**Description**: ${currentCommand.getDescr()}\n**Usage**: ${currentCommand.getUsage()}\n\n`);
            }
            message.author.send(helpDM);
            message.channel.send(`<@${message.author.id}>, Check your DM for a list of available commands`);
            break;
        case CommandInterpreter.Command.MOTIVATE:
            const quote = ArnoldQuoteRetriever.getQuote();
            message.channel.send(quote);
            break;
        case CommandInterpreter.Command.AUTO_MOTIVATE:
            if(message.guild.ownerID !== message.author.id) {
                break;
            }
            if (serverAutoMotivateMap[message.guild]) {
                message.channel.send("Quotes are already being sent.");
                return;
            }
            if (isNaN(Number(arguments[1]))) {
                message.channel.send("Invalid interval. Enter a number, you idiot.")
                return;
            }
            const interval = Math.min(Number(arguments[1]), config.maxTime) * 1000;
            message.channel.send(`Sending a quote by Arnold every ${arguments[1]} seconds.`);
            message.channel.send(ArnoldQuoteRetriever.getQuote());
            arguments[1] = interval.toString();
            serverAutoMotivateMap[message.guild] = setInterval(() => {
                message.channel.send(ArnoldQuoteRetriever.getQuote());
            }, arguments[1]);
            break;
        case CommandInterpreter.Command.STOP_MOTIVATING:
            if(message.guild.ownerID !== message.author.id) {
                break;
            }
            if (serverAutoMotivateMap[message.guild]) {
                clearInterval(serverAutoMotivateMap[message.guild])
                delete serverAutoMotivateMap[message.guild];
                message.channel.send("I'll be BACK... but for now I won't send anymore quotes.");
            } else {
                message.channel.send("I'm not sending any motivational quotes at the moment.");
            }
            break;
        case CommandInterpreter.Command.LARRY:
            message.channel.send("COME ON LARRRYYYYYYYYYYYYYY!");
            break;
        default:
            message.channel.send("Invalid command, type !help for a list of available commands");
    }
});

client.on("error", (error) => {
    console.log(`Error! ${error.message}`);
});

client.login(process.env.token);