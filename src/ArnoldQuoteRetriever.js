const config = require('./config.js');
class ArnoldQuoteRetriever {
    getQuote() {
        const index = Math.random() * config.quotes.length;
        return config.quotes[Math.floor(index)];
    }
}
module.exports = new ArnoldQuoteRetriever();