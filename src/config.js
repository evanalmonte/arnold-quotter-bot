const dotenv = require('dotenv');
const fs = require("fs");
const configJSON = JSON.parse(fs.readFileSync("src/config.json"));
dotenv.config();

module.exports = {
    serverGreeting: configJSON["serverGreeting"],
    quotes: JSON.parse(fs.readFileSync(`src/config.json`))["arnold_quotes"],
    maxTime: configJSON["max_time"]
}